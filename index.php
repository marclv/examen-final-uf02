<?php

include("include/header.php");


require(__DIR__ . '/lib/HTTPClient.php');
require(__DIR__ . '/lib/JSONParser.php');
$a = 0;
####################################################
####################################################
########### EXEMPLE client HTTP ###################
####################################################
####################################################

# Primer instanciar la classe, amb la base URL on hi ha la nostre api
$base_url = 'http://127.0.0.1:3000/';
$token = 'C0UsWlYxXrMx81TKN2Eq';
$client = new HTTPClient($base_url, $token);


# Si necessitem enviar paramatres a l'API, primer crearem un array amb els values
##$params = [
##'camp1' => 'xxxxxxxxx',
##'camp2' => 'xxxxxxxxx',
##];

## Mitjançant el client instanciat anteriorment, podem realitzar crides a l'API amb varis metodes
## i també enviar parametres.
# $client->query(string $uri, array $params = [], string $method='GET')
# Aquesta petició sempre ens retornarà un array amb dos camps:
## status = true / false
## data = retorn de l'API
##$result = $client->query('endpoint');
##$result = $client->query('endpoint', $params, 'POST');
##$result = $client->query('endpoint', $params, 'PUT');
##$result = $client->query('endpoint', [], 'DELETE');

####################################################
####################################################
########### EXEMPLE parser JSON ###################
####################################################
####################################################

##$result = JSONParser::parseFile(__DIR__ . '/alumnes.json');
$boton1 = "";
$boton2 = "";
$boton3 = "";
$boton4 = "";
if (isset($_POST['Insert1']))  $boton1 = $_POST['Insert1'];
if (isset($_POST['Insert2'])) $boton2 = $_POST['Insert2'];
if (isset($_POST['Insert3'])) $boton3 = $_POST['Insert3'];
if (isset($_POST['Insert4'])) $boton4 = $_POST['Insert4'];


if ($boton1 == "Insert Json Alumnes") {
    $a = 1;
    try {
        $datos = $client->query('/api/v1/alumnes');


        $result = JSONParser::parseFile(__DIR__ . '/alumnes.json');
        foreach ($result['data'] as $data) {
            $data = get_object_vars($data);
            $param = [
                'nom' => $data['nom'],
                'cognoms' => $data['cognoms'],
                'age' => $data['age'],
                'gender' => $data['gender'],
                'mail' => $data['mail'],
                'telefon' => $data['telefon'],
                'registered' => $data['registered'],
            ];


            $result = $client->query('/api/v1/alumne', $param, 'POST');
            if ($result['data']['code'] == 'Code error: 201') {

                $_SESSION['message'] = 'Alumnes inserits correctament';
                $_SESSION['message_tyipe'] = 'success';
            } else {

                $_SESSION['message'] = $result['data']['code'];
                $_SESSION['message_tyipe'] = 'danger';
            }
        }
    } catch (Exception $e) {
        $_SESSION['message'] = 'Error';
        $_SESSION['message_tyipe'] = 'danger';
    }
}
if ($boton2 == "Insert Json Assignatures") {
    $a = 2;
    try {
        $datos2 = $client->query('/api/v1/assignatures');
        $result = JSONParser::parseFile(__DIR__ . '/assignatures.json');
        foreach ($result['data'] as $data) {
            $data = get_object_vars($data);
            $param = [
                'nom' => $data['nom'],
                'professor' => $data['professor'],
            ];

            $result = $client->query('/api/v1/assignatura', $param, 'POST');

            if ($result['data']['code'] == 'Code error: 201') {

                $_SESSION['message'] = 'Assignatures inserides correctament';
                $_SESSION['message_tyipe'] = 'success';
            } else {

                $_SESSION['message'] = $result['data']['code'];
                $_SESSION['message_tyipe'] = 'danger';
            }
        }
    } catch (Exception $e) {
        $_SESSION['message'] = 'Error';
        $_SESSION['message_tyipe'] = 'danger';
    }
}

if ($boton3 == "Insert Alumnes-Assigantures") {
    $a = 3;
    try {
        $datos3 = $client->query('/api/v1/alumnes');
        $datos4 = $client->query('/api/v1/assignatures');

        foreach ($datos3['data'] as $data) {
            $param = [

                'alumne_id' => $data->id,
                'assignatura_id' => $datos4['data'][0]->id

            ];
            $result = $client->query('/api/v1/vincular', $param, 'POST');
        }

        foreach ($datos3['data'] as $data) {
            $param = [

                'alumne_id' => $data->id,
                'assignatura_id' => $datos4['data'][1]->id

            ];
            $result = $client->query('/api/v1/vincular', $param, 'POST');
        }
    } catch (Exception $e) {
        $_SESSION['message'] = 'Error';
        $_SESSION['message_tyipe'] = 'danger';
    }
}

if ($boton4 == "Insert Json Notes") {
    $a = 4;
    try {


        $datos3 = $client->query('/api/v1/alumnes');
        $datos4 = $client->query('/api/v1/assignatures');

        foreach ($datos3['data'] as $data) {
            $param = [

                'alumne_id' => $data->id,
                'assignatura_id' => $datos4['data'][0]->id,
                'nota' => 5

            ];
            $result = $client->query('/api/v1/nota', $param, 'POST');
        }

        foreach ($datos3['data'] as $data) {
            $param = [

                'alumne_id' => $data->id,
                'assignatura_id' => $datos4['data'][1]->id,
                'nota' => 5

            ];
            $result = $client->query('/api/v1/nota', $param, 'POST');
        }
    } catch (Exception $e) {
        $_SESSION['message'] = 'Error';
        $_SESSION['message_tyipe'] = 'danger';
    }
}


## $result contindrà dos camps
## status = true / false

?>


<div class="container p-4">
    <div class="row">
        <div class="col-md-4">

            <?php

            if (isset($_SESSION['message'])) {  ?>
            <div class="alert alert-<?= $_SESSION['message_tyipe'] ?> alert-dismissible fade show" role="alert">
                <?= $_SESSION['message'] ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php session_unset();
        }
        ?>
            <div class="card card-body">

                <form method="post">

                    <input type="submit" class="btn btn-success btn-block" name="Insert1" value="Insert Json Alumnes">
                    <input type="submit" class="btn btn-success btn-block" name="Insert2" value="Insert Json Assignatures">
                    <input type="submit" class="btn btn-success btn-block" name="Insert3" value="Insert Alumnes-Assigantures">
                    <input type="submit" class="btn btn-success btn-block" name="Insert4" value="Insert Json Notes">



                </form>
            </div>
        </div>

        <?php if ($a == 1) { ?>

        <div class="col-md-8">
            <table class="table table-bordered">

                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nom</th>
                        <th>Cognoms</th>
                        <th>Mail</th>
                        <th>CreatedAt</th>
                        <th>UpdatedAt</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                    foreach ($datos['data'] as $data) {
                        echo "<tr>";
                        echo "<td>$data->id</td>";
                        echo "<td>$data->nom</td>";
                        echo "<td>$data->cognoms</td>";
                        echo "<td>$data->mail</td>";
                        echo "<td>$data->createdAt</td>";
                        echo "<td>$data->updatedAt</td>";
                        echo '<td> <a href="delet_alumnes.php?id=' . $data->id . '" class= "btn btn-danger">
                <i class= "far fa-trash-alt" > </i> </td> ';
                        echo "</tr>";
                    }

                    ?>

                </tbody>

            </table>
        </div>
        <?php 
    } ?>
        <?php if ($a == 2) { ?>
        <div class="col-md-8">
            <table class="table table-bordered">

                <thead>
                    <tr>
                        <th>id</th>
                        <th>nom</th>
                        <th>professor</th>
                        <th>createdAt</th>
                        <th>updatedAt</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                    foreach ($datos2['data'] as $data) {
                        echo "<tr>";
                        echo "<td>$data->id</td>";
                        echo "<td>$data->nom</td>";
                        echo "<td>$data->professor</td>";
                        echo "<td>$data->createdAt</td>";
                        echo "<td>$data->updatedAt</td>";
                        echo '<td> <a href="delet_assignatures.php?id=' . $data->id . '" class= "btn btn-danger">
                <i class= "far fa-trash-alt" > </i> </td> ';
                        echo "</tr>";
                    }

                    ?>

                </tbody>

            </table>
        </div>
        <?php 
    } ?>

    </div>
</div>

<?php include("include/footer.php"); ?> 